import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };
export class HomeComponent implements OnInit {
  isPaymentSection: boolean = false;
  isStripeFormVisbile: boolean = false;
  isShowPlanList: boolean = false;
  stripToken: string = "";
  planList: array = [];
  selectedPlanId: string = "";
  customerDetails: any;
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.loadAllPlans();
    this.loadCustomer();
  }

  loadCustomer() {
    this.http.get('http://127.0.0.1:8000/api/get-customer').subscribe((data:any)=>{
      console.log(data)
      this.customerDetails = data.customer
    }, (err:any)=>{
      console.log(err)
    })
  }

  loadAllPlans() {
    this.http.get('http://127.0.0.1:8000/api/get-plan-list').subscribe((data:any)=>{
      console.log(data)
      this.planList = data.list
    }, (err:any)=>{
      console.log(err)
    })
  }

  showStripeForm() {
    this.isStripeFormVisbile = true;
    this.isPaymentSection = true;
  }

  getStripToken(_token: string) {
    this.stripToken = _token
  }

  showPlanList(_isShowPlanList: boolean) {
    this.isShowPlanList = _isShowPlanList
  }

  changeSelectPlan() {
    this.selectedPlanId = document.querySelector("select[name='selectedPlan']").value;
  }

  submitToken() {
    if(!this.selectedPlanId) {
      alert("Please select a plan")
    } else {
      let _formvalues = {}
      _formvalues.token = this.stripToken;
      _formvalues.customer_id = this.customerDetails.acc_id;
      _formvalues.plan_id = this.selectedPlanId;
      this.http.post('http://127.0.0.1:8000/api/generate-customer-payment-source', _formvalues).subscribe((data:any)=>{
        console.log(data)
      }, (err:any)=>{
        console.log(err.error)
        alert(err.error.error)
      })
    }
  }

}
