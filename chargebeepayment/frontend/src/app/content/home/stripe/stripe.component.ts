import { Component, OnInit, Input, Output, AfterViewInit, OnChanges, EventEmitter } from '@angular/core';

declare var stripe: any;
declare var elements: any;

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.css']
})
export class StripeComponent implements OnInit, OnChanges {
  @Input('stripeVisible') stripeVisible: boolean
  @Output() stripeTokenEmit = new EventEmitter<string>();
  @Output() showPlanListEmit = new EventEmitter<string>();

  cardNumber: any;
  cardExpiry: any;
  cardCvc: any;
  isComplete: boolean = false;
  stripeToken: any;
  showPlanList: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if(this.stripeVisible === true) {
      setTimeout(() => {
        this.mountStripeElement();
      },200);
    }
  }

  mountStripeElement() {
    var elements = stripe.elements({
      fonts: [
        {
          cssSrc: 'https://fonts.googleapis.com/css?family=Quicksand',
        },
      ]
    });

    var elementStyles = {
      base: {
        color: '#fff',
        fontWeight: 600,
        fontFamily: 'Quicksand, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',

        ':focus': {
          color: '#424770',
        },

        '::placeholder': {
          color: '#9BACC8',
        },

        ':focus::placeholder': {
          color: '#CFD7DF',
        },
      },
      invalid: {
        color: '#fff',
        ':focus': {
          color: '#FA755A',
        },
        '::placeholder': {
          color: '#FFCCA5',
        },
      },
    };

    var elementClasses = {
      focus: 'focus',
      empty: 'empty',
      invalid: 'invalid',
    };

    this.cardNumber = elements.create('cardNumber', {
      style: elementStyles,
      classes: elementClasses,
    });
    this.cardNumber.mount('#example3-card-number');

    this.cardExpiry = elements.create('cardExpiry', {
      style: elementStyles,
      classes: elementClasses,
    });
    this.cardExpiry.mount('#example3-card-expiry');

    this.cardCvc = elements.create('cardCvc', {
      style: elementStyles,
      classes: elementClasses,
    });
    this.cardCvc.mount('#example3-card-cvc');

    const cardElements = [this.cardNumber,this.cardExpiry,this.cardCvc];

    var example = document.querySelector('.example3');
    var form = example.querySelector('form');

    var error = form.querySelector('.error');
    var errorMessageElement = <HTMLElement> error.querySelector('.message');

    var success = form.querySelector('.success');
    var successMessageElement = <HTMLElement> success.querySelector('.message');

    // validation of card number
    cardElements.forEach((element) => {
      element.on('change', function(event) {
        if(event.error) {
          console.error("Error in card details",event);
          success.classList.add('hide');
          error.classList.remove('hide');
          errorMessageElement.innerText = event.error.message;
        }
        else if(event.complete === true && event.elementType === 'cardNumber') {
          cardElements[1].focus();
          success.classList.add('hide');
          error.classList.add('hide');
        }
        else if(event.complete === true && event.elementType === 'cardExpiry') {
          cardElements[2].focus();
          success.classList.add('hide');
          error.classList.add('hide');
        }
        else if(event.complete === true && event.elementType === 'cardCvc') {
          cardElements[2].blur();
          success.classList.add('hide');
          error.classList.add('hide');
        }
      });
    })

  }

  generateToken() {
    this.registerForToken([this.cardNumber,this.cardExpiry,this.cardCvc]);
  }

  async registerForToken(cardElements: any[]) {
    // var _this = this;
    var example = document.querySelector('.example3');
    var form = example.querySelector('form');

    var error = form.querySelector('.error');
    var errorMessageElement = <HTMLElement> error.querySelector('.message');

    var success = form.querySelector('.success');
    var successMessageElement = <HTMLElement> success.querySelector('.message');
    var TokenElement = <HTMLElement> successMessageElement.querySelector('.token');

    var stripeForm = form.querySelector('.fieldset');

    const additionalData = {
    };

    await stripe.createToken(cardElements[0],additionalData)
    .then((tokenDetails: any) => {
      console.log(tokenDetails);
      if(tokenDetails.error) {
        success.classList.add('hide');
        error.classList.remove('hide');
        errorMessageElement.innerText = tokenDetails.error.message;
      }
      else {
        sendTokenDetailsToParent()
        this.stripeToken = tokenDetails.token.id
        success.classList.remove('hide');
        error.classList.add('hide');
        TokenElement.innerText = tokenDetails.token.id;
        stripeForm.classList.add('hide');
        this.isComplete = true;
      }
    })
  }

  sendTokenDetailsToParent() {
    this.stripeTokenEmit.emit(this.stripeToken)
    this.showPlanListEmit.emit(this.showPlanList)
  }


}
