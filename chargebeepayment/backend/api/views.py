from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from api.models import Plan, ParentAccount
import chargebee
import json
from datetime import date, datetime
# chargebee.configure("test_HLLaldyLX8ViDVAu9EZeku76fikZaMVc","aeiser-test")
chargebee.configure("live_RHvYbbvZhDaHcuaA8cFU2J8UCIB0lMuVv","aeiser")
# Create your views here.

def testIndex(request):
    return HttpResponse("<h1>Server run successfully</h1>");

def createCustomerPlanSubscription():
    CUSTOMER_ID = ""
    # PLAN_ID = "_plan_250_dolar123"
    # # SUBSCRIPTION_ID = None
    # SUBSCRIPTION_ID = None
    # Createing customer-----------------------------------
    try:
        # _customerIns = chargebee.Customer.retrieve("6oku3RoDmxNM1EH4")
        # CUSTOMER_ID = _customerIns.customer.id
        # print("Customer Id: ", CUSTOMER_ID)
        # print("___________________")
        # print("Customer: ", _customerIns);

        # retrive payment source
        _chargebeePaymentSource = chargebee.PaymentSource.retrieve("pm_Azyw6ERoDnz279B7")
        payment_source = _chargebeePaymentSource.payment_source
        print("Payment Source: ", _chargebeePaymentSource.payment_source)
        print("____________________________________________")
        print("Payment Source: ", _chargebeePaymentSource)
    except chargebee.APIError as e:
        print(e)
    # try:
    #     result = chargebee.PaymentSource.create_using_temp_token({
    #             "customer_id" : "169yJtRo81FdDKML",
    #             "type" : "card",
    #             "tmp_token" : "tok_1G3LqOJBpAWvaHb5IFdkYytl"
    #         })
    #     customer = result.customer
    #     payment_source = result.payment_source
    #     print(result)
    # except chargebee.APIError as e:
    #      print(e)

    # try:
        # result = chargebee.Customer.create({
        #     "first_name" : "Shyamojjwal",
        #     "last_name" : "Shit",
        #     "email" : "shyamojjwal@yopmail.com"
        #     })
        # result = chargebee.PaymentSource.create_using_temp_token({
        #     "gateway_account_id" : "gw_16CPWbRmftucaO8",
        #     "customer_id" : "6oku3RoDmxNM1EH4",
        #     "type" : "card",
        #     "tmp_token" : "tok_1G3Ni2JBpAWvaHb5f2gLid6X"
        # })
        # result = chargebee.PaymentSource.create_using_payment_intent({
        #     "customer_id" : "6oku3RoDmxNM1EH4",
        #     "payment_intent" : {
        #         "gateway_account_id" : "gw_16CPWbRmftucaO8",
        #         "gw_token" : "tok_1G3Ni2JBpAWvaHb5f2gLid6X"
        #         }
        # })
        # customer = result.customer
        # payment_source = result.payment_source
        # print("result: ", result)
        # print("______________________________ ")
        # print("customer: ", customer)
        # entries = chargebee.Plan.list({
        #     "status[is]" : "active"
        # })
        # for entry in entries:
        #     _planDetails = entry.plan
        #
        #     Plan.objects.create(
        #         plan_id = _planDetails.id,
        #         plan_name = _planDetails.name,
        #         price = _planDetails.price,
        #         period = _planDetails.period,
        #         period_unit = _planDetails.period_unit,
        #         status = _planDetails.status
        #     )
    # except chargebee.APIError as e:
    #     print(e)
    # pass


createCustomerPlanSubscription()


class getPlanList(GenericAPIView):
    authentication_classes = ()
    permission_classes = ()

    @classmethod
    def get(self, request):
        res = {}
        res["isError"] = False
        res["list"] = [{
                "id": _plan.plan_id,
                "name": _plan.plan_name,
                "description": _plan.description,
                "price": _plan.price,
                "period": _plan.period,
                "period_unit": _plan.period_unit,
            } for _plan in Plan.objects.filter(status__iexact='active').all()
        ]
        return Response(res, status=200)


class getCustomer(GenericAPIView):
    authentication_classes = ()
    permission_classes = ()

    @classmethod
    def get(self, request):
        res = {}
        _customer = ParentAccount.objects.filter().last()
        if _customer:
            res["isError"] = False
            res["customer"] = {
                    "acc_id": _customer.account_id,
                    "parent_id": _customer.parent_id,
                    "name": _customer.name,
                    "parent_type": _customer.parent_type,
                    "contact": _customer.contact,
                    "email": _customer.email
                }
            return Response(res, status=200)
        else:
            res["isError"] = True
            res["error"] = "Plese add a customer"
            return Response(res, status=400)


class generateCustomerPaymentSource(GenericAPIView):
    authentication_classes = ()
    permission_classes = ()

    @classmethod
    def post(self, request):
        res = {}
        # print(request.data)
        _tempToken = request.data.get("token", None)
        _customer_id = request.data.get("customer_id", None)
        _plan_id = request.data.get("plan_id", None)

        if _tempToken and _customer_id and _plan_id:
            res["tempToken"] = _tempToken
            res["customer_id"] = _customer_id
            res["plan_id"] = _plan_id

            # Checking Customer already created or not at chargebee
            try:
                _apiRes = chargebee.Customer.retrieve("6oku3RoDmxNM1EH4")
                _customerIns = _apiRes.customer
            except chargebee.APIError:
                _apiRes = chargebee.Customer.retrieve("6oku3RoDmxNM1EH4")
                _chargeBeeCust = _apiRes.customer
                # Creating Payment source through temp token
                _apiResPaymentSource = chargebee.PaymentSource.create_using_payment_intent({
                    "customer_id" : _chargeBeeCust.id,
                    "payment_intent" : {
                        "gateway_account_id" : "gw_16CPWbRmftucaO8",
                        "gw_token" : _tempToken
                    }
                })
                _chargebeePaymentSource = _apiResPaymentSource.payment_source
                # Inserting Payment Source details to db
                _paymentSourceIns = CustomerPaymentSource.objects.create(
                    payment_source_id = _chargebeePaymentSource.id,
                    customer_id = _chargebeePaymentSource.customer_id,
                    # token_id = _chargebeePaymentSource.id,
                    reference_id = _chargebeePaymentSource.reference_id,
                    card_type = _chargebeePaymentSource.type,
                    status = _chargebeePaymentSource.status,
                    gateway = _chargebeePaymentSource.gateway,
                    gateway_account_id = _chargebeePaymentSource.gateway_account_id,
                    # ip_address = _chargebeePaymentSource.ip_address,
                    created_at = _chargebeePaymentSource.created_at,
                    updated_at = _chargebeePaymentSource.updated_at
                )

                # Inserting customer record to customer table
                _customerIns = Customer.objects.create(
                    customer_id=_chargeBeeCust.id,
                    first_name=_chargeBeeCust.first_name,
                    last_name=_chargeBeeCust.last_name,
                    email=_chargeBeeCust.email,
                    auto_collection=_chargeBeeCust.auto_collection,
                    preferred_currency_code=_chargeBeeCust.preferred_currency_code,
                    # primary_account_source=_chargeBeeCust.primary_account_source,
                    created_at=_chargeBeeCust.created_at,
                    updated_at=_chargeBeeCust.updated_at
                )

            return Response(res, status=200)
        else:
            res["isError"] = True
            res["error"] = "Plese check your details"
            return Response(res, status=400)
