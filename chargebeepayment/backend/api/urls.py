from django.urls import path, include
from api.views import *

urlpatterns = [
    path('test/', testIndex, name="test"),
    path('get-plan-list', getPlanList.as_view(), name="get-plan-list"),
    path('get-customer', getCustomer.as_view(), name="get-customer"),
    path('generate-customer-payment-source', generateCustomerPaymentSource.as_view(), name="generate-customer-payment-source"),
]
