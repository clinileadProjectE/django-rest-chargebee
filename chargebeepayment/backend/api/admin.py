from django.contrib import admin
from .models import *
admin.site.register(AccountData)
admin.site.register(ParentAccount)
admin.site.register(ParentAddress)
admin.site.register(StudentAccount)
