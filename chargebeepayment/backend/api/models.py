from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
class AccountData(AbstractUser):
    account_id = models.CharField(default='_7485fgt', max_length=50, primary_key=True, null=False, blank=True)

class ParentAccount(models.Model):
    PARENT_TYPE = (
                        ('father', 'Father'),
                        ('mother', 'Mother')
                    )
    account = models.ForeignKey(AccountData, related_name="parent_account", on_delete=models.CASCADE, null=True)
    parent_id = models.CharField(max_length=8, null=False, blank=True, primary_key=True)
    name = models.CharField(max_length=250, null=True, blank=True)
    parent_type = models.CharField(
            verbose_name="Parent Type", choices=PARENT_TYPE,
            max_length=25, default="father", null=True
        )
    contact = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=250, null=True, blank=True)
    email_verified_status = models.BooleanField(default=False)
    phone_verified_status = models.BooleanField(default=False)

class ParentAddress(models.Model):
    account = models.ForeignKey(AccountData, related_name="parent_address", on_delete=models.CASCADE, null=True)
    address1 = models.CharField(max_length=250, null=True, blank=True)
    address2 = models.CharField(max_length=250, null=True, blank=True)
    postal_code = models.CharField(max_length=10, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)

class StudentAccount(models.Model):
    GENDER_TYPE = (
                    ('male', 'Male'),
                    ('female', 'Female')
                )
    account = models.ForeignKey(AccountData, related_name="student_accounts", on_delete=models.CASCADE, null=True)
    student_id = models.CharField(max_length=250, null=False, blank=True, primary_key=True)
    student_name = models.CharField(max_length=250, null=True, blank=True)
    nick_name = models.CharField(max_length=50, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(
            verbose_name="Parent Type", choices=GENDER_TYPE,
            max_length=25, default="male", null=True
        )
    school = models.CharField(max_length=250, null=True, blank=True)
    subscription_id = models.CharField(max_length=250, null=True, blank=True) #Foregn key to CustomerSubscription
    # package_id = models.CharField(max_length=250, null=True, blank=True)

class Plan(models.Model):
    plan_id = models.CharField(max_length=250, blank=True, primary_key=True)
    plan_name = models.CharField(max_length=250, null=True, blank=True)
    description = models.TextField(null=True, blank=True, default=None)
    price = models.FloatField(null=True, blank=True)
    period = models.IntegerField(default=1,null=True, blank=True)
    period_unit = models.CharField(default=None, max_length=250,null=True, blank=True)
    status = models.CharField(default=None, max_length=250,null=True, blank=True)

class CustomerPaymentSource(models.Model):
    payment_source_id = models.IntegerField(default=None, null=False, blank=True, primary_key=True)
    customer = models.ForeignKey(AccountData, related_name="customer_payment_source", on_delete=models.CASCADE, null=True)
    token_id = models.CharField(default=None, max_length=250, null=True, blank=True)
    reference_id = models.CharField(default=None, max_length=250, null=True, blank=True)
    card_type = models.CharField(default=None, max_length=250, null=True, blank=True)
    status = models.CharField(default=None, max_length=10, null=True, blank=True)
    gateway = models.CharField(default=None, max_length=250, null=True, blank=True)
    gateway_account_id = models.CharField(default=None, max_length=250, null=True, blank=True)
    ip_address = models.CharField(default=None, max_length=250, null=True, blank=True)
    created_at = models.IntegerField(null=True, blank=True, default=None)
    updated_at = models.IntegerField(null=True, blank=True, default=None)

class Customer(models.Model):
    customer = models.ForeignKey(AccountData, related_name="customer_account", on_delete=models.CASCADE, null=True)
    first_name = models.CharField(max_length=250, null=True, blank=True, default=None)
    last_name = models.CharField(max_length=250, null=True, blank=True, default=None)
    email = models.CharField(max_length=250, null=True, blank=True, default=None)
    billing_address = models.CharField(max_length=250, null=True, blank=True, default=None)
    auto_collection = models.CharField(max_length=250, null=True, blank=True, default=None)
    preferred_currency_code = models.CharField(max_length=250, null=True, blank=True, default=None)
    primary_account_source = models.ForeignKey(CustomerPaymentSource, related_name="customer_details", on_delete=models.CASCADE, null=True)
    created_at = models.IntegerField(null=True, blank=True, default=None)
    updated_at = models.IntegerField(null=True, blank=True, default=None)

class CustomerSubscription(models.Model):
    subscription_id = models.CharField(default=None, max_length=50, null=False, blank=True, primary_key=True)
    customer = models.ForeignKey(AccountData, related_name="customer_subscription", on_delete=models.CASCADE, null=True)
    plan_id = models.ForeignKey(Plan, related_name="plan_subscription", on_delete=models.CASCADE, null=True)
    payment_source = models.ForeignKey(CustomerPaymentSource, related_name="payment_source_subscription", on_delete=models.CASCADE, null=True)
    status = models.CharField(default=None, max_length=50, null=True, blank=True)
    next_billing_at = models.IntegerField(default=None, null=True, blank=True)
    currency_code = models.CharField(default=None, max_length=50, null=True, blank=True)
    chargebee_deleted_at = models.CharField(default=None, max_length=50, null=True, blank=True)
    reactivated_at = models.CharField(default=None, max_length=50, null=True, blank=True)
    started_at = models.CharField(default=None, max_length=50, null=True, blank=True)
    created_at = models.IntegerField(null=True, blank=True, default=None)
    updated_at = models.IntegerField(null=True, blank=True, default=None)

class CustomerInvoice(models.Model):
    invoice_id = models.CharField(default=None, max_length=50, null=False, blank=True, primary_key=True)
    customer = models.ForeignKey(AccountData, related_name="customer_invoice", on_delete=models.CASCADE, null=True)
    subscription = models.ForeignKey(CustomerSubscription, related_name="customer_subscription_invoice", on_delete=models.CASCADE, null=True)
    transaction_id = models.CharField(default=None, max_length=250, null=True, blank=True)
    status = models.CharField(default=None, max_length=50, null=True, blank=True)
    price_type = models.CharField(default=None, max_length=50, null=True, blank=True)
    date = models.IntegerField(null=True, blank=True, default=None)
    due_date = models.IntegerField(null=True, blank=True, default=None)
    chargebee_deleted_at = models.CharField(default=None, max_length=50, null=True, blank=True)
    paid_at = models.IntegerField(null=True, blank=True, default=None)
    amount_paid = models.IntegerField(null=True, blank=True, default=None)
    created_at = models.IntegerField(null=True, blank=True, default=None)
    updated_at = models.IntegerField(null=True, blank=True, default=None)
